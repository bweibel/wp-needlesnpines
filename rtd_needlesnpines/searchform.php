<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
    <input type="search" id="s" name="s" value="" placeholder="SEARCH.." />

    <button type="submit" id="searchsubmit" value="Search" title="Search {{RTD-BASE-THEME}}" alt="Search {{RTD-BASE-THEME}}"></button>
</form>
