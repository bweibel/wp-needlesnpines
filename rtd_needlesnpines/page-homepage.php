<?php
/*
    Template Name: Site Homepage
*/

    get_header(); ?>

            <div class="homepage-wrap" id="content">

                <div id="inner-content-main" class=" cf">

                    <?php get_template_part( 'partials/featured_image' ); ?>

                    <div class="homepage-welcome ">
                        <p class="blurb"><?php echo get_field('intro_blurb'); ?></p>
                        <a href="<?php echo get_field('button_link'); ?>" class="needles-button-light"><?php echo get_field('button_text'); ?></a>
                    </div>

                    <main id="main" class="" role="main" itemscope itemprop="mainContentOfPage">

                        <section class="wrap cf" itemprop="articleBody">

                            <div class="page-content">

                                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                                <?php the_content(); ?>

                                <?php endwhile; else : ?>

                                <h1><?php _e( 'Homepage content not found!', 'rtdtheme' ); ?></h1>

                                <?php endif; ?>

                            </div>

                        </section>

                    </main>

                </div>

            </div>


<?php get_footer(); ?>
