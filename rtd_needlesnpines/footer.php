            <footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

                <div id="inner-footer" class="wrap cf">

                    <div class="footer-main-row cf">

                        <div class="footer-first m-all t-1of3 d-1of3">
                            <?php if ( is_active_sidebar( 'footerarea1' ) ) : ?>

                                <?php dynamic_sidebar( 'footerarea1' ); ?>

                            <?php else : ?>

                                <div class="no-widgets">
                                </div>

                            <?php endif; ?>
                        </div>

                        <div class="footer-middle m-all t-1of3 d-1of3">
                            <?php if ( is_active_sidebar( 'footerarea2' ) ) : ?>

                                <?php dynamic_sidebar( 'footerarea2' ); ?>

                            <?php else : ?>

                                <div class="no-widgets">
                                </div>

                            <?php endif; ?>
                        </div>

                        <div class="footer-last m-all t-1of3 d-1of3">
                            <?php if ( is_active_sidebar( 'footerarea3' ) ) : ?>

                                <?php dynamic_sidebar( 'footerarea3' ); ?>

                            <?php else : ?>

                                <div class="no-widgets">
                                </div>

                            <?php endif; ?>
                        </div>

                    </div>

                    <div class="footer-copyright">

                        <p class="source-org copyright">&copy; <?php echo date('Y'); ?> <a href="<?php echo home_url(); ?>" rel="nofollow" title="Click here to return to the homepage!">Needles N Pines</a>, All Rights Reserved. <a href="/wp-admin/" target="_blank">Site Admin</a></p>

                    </div>

                    <div class="credit">

                        <!-- <a href="https://redtaildesignco.com/" target="_blank"><img src="<?php // echo get_template_directory_uri(); ?>/library/images/made-locally-byev_dark.png"></a> -->

                    </div>

                    <div class="footer-overlay"></div>

                </div>

            </footer>

        </div>

        <?php // all js loaded in library/bones.php ?>
        <?php wp_footer(); ?>

    </body>

</html> <!-- end of site. -->
