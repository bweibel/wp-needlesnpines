<?php get_header(); ?>

            <div class="standardpage-wrap search-results" id="content">

                <div id="inner-content" class="wrap cf">

                    <main id="main" class="standard-content m-all t-2of3 d-3of4 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

                        <?php
                            if ( function_exists('yoast_breadcrumb') ) {
                                yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                            }
                        ?>

                        <h1 class="archive-title"><span><?php _e( 'Search Results for:', 'rtdtheme' ); ?></span> "<?php echo esc_attr(get_search_query()); ?>"</h1>

                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                            <article id="post-<?php the_ID(); ?>" <?php post_class('page-content cf'); ?> role="article">

                                <h2 class="search-title entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>

                                <div class="excerpt-content">
                                    <?php echo improved_trim_excerpt('',75); ?>
                                </div>

                                <span class="standard-horiz-dashed-line"></span>

                            </article>

                        <?php endwhile; ?>

                        <?php bones_page_navi(); ?>

                        <?php else : ?>

                            <article id="post-not-found" class="hentry cf">
                                <header class="article-header">
                                    <h2><?php _e( 'Sorry, No Results.', 'rtdtheme' ); ?></h2>
                                </header>
                                <section class="entry-content">
                                    <p><?php _e( 'Try your search again.', 'rtdtheme' ); ?></p>
                                </section>
                            </article>

                        <?php endif; ?>

                    </main>

                    <?php get_sidebar(); ?>

                </div>

            </div>

<?php get_footer(); ?>
