<?php
/*
    Template Name: Full-Width (no sidebar)
*/
    get_header(); ?>

            <div class="standardpage-wrap" id="content">

                <div id="inner-content" class="wrap cf">

                    <div class="page-container m-all t-all d-all">

                        <?php get_template_part( 'partials/featured_image' ); ?>

                        <main id="main" class="standard-content m-all t-all d-all cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

                            <?php if ( function_exists('yoast_breadcrumb') ) {
                                yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                            } ?>

                            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                            <article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">

                                <header class="page-header">

                                    <h1 class="page-title"><?php the_title(); ?></h1>

                                </header>

                                <section class="page-content cf" itemprop="articleBody">
                                    <?php the_content(); ?>
                                </section>

                            </article>

                            <?php endwhile; else : ?>

                                    <article id="post-not-found" class="hentry cf">
                                        <header class="article-header">
                                            <h1><?php _e( 'Page content!', 'rtdtheme' ); ?></h1>
                                        </header>
                                        <section class="entry-content">
                                            <p><?php _e( 'Double check settings and configuration.', 'rtdtheme' ); ?></p>
                                        </section>
                                    </article>

                            <?php endif; ?>

                        </main>

                    </div>

                </div>

            </div>

<?php get_footer(); ?>
