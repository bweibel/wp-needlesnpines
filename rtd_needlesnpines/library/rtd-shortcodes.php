<?php
/*
Title: RTD Shortcodes
Author: Ben Weibel
URL: https://redtaildesignco.com/
*/

// [rtd_button] SHORTCODE (be sure to adjust SCSS in _buttons.scss accordingly) (https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Shortcodes#markdown-header-ev-buttons)
    function rtd_button_func( $atts, $content = null ) {
        $a = shortcode_atts( array(
            'text'      => 'Button Text',
            'link'      => '/',
            'color'     => 'red',
            'big'       => 'false'
        ), $atts );

        if ( $a['color'] == 'red' ) {
            if ( $a['big'] == 'true' ) {
                return '<a class="in-page-button-big-red" href="' . $a['link'] . '">' . $a['text'] . '</a>';
            } else {
                return '<a class="in-page-button-regular" href="' . $a['link'] . '">' . $a['text'] . '</a>';
            }
        } else {
            if ( $a['big'] == 'true' ) {
                return '<a class="in-page-button-big-gray" href="' . $a['link'] . '">' . $a['text'] . '</a>';
            } else {
                return '<a class="in-page-button-gray" href="' . $a['link'] . '">' . $a['text'] . '</a>';
            }
        }
    }
    add_shortcode( 'rtd_button', 'rtd_button_func' );

    function map_embed_func(){
        return '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3080.1711089140185!2d-82.24290868396636!3d39.46546297948696!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8847e18df4ba1991%3A0x740910f086ab2f5d!2s149+Pine+Grove+Dr%2C+Nelsonville%2C+OH+45764!5e0!3m2!1sen!2sus!4v1503923299150" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>';
    }
    add_shortcode( 'map', 'map_embed_func');

    function image_bg_func($atts, $content = null){
        return '<section class="image-bg">' . $content . '</section>';
    }
    add_shortcode( 'image_bg', 'image_bg_func');

function faq_func($atts){
    global $post, $wpdb;

    $sql = sprintf(
        '   SELECT      `ID`,
                        `post_title`,
                        `menu_order`
            FROM        `%s`
            WHERE       `post_type` = "needles_faq"
            ORDER BY    `menu_order`, `post_title`',
        $wpdb->posts

    );
    if ( $faqs = $wpdb->get_results($sql) ) {
        $html = null;
        foreach ($faqs as $faq) {
            // $current = ($sibling->ID == $post->ID);
            $html .= sprintf(
                '<h4 class="question"><a>%s</a></h4><div class="answer">%s</div>',
                // $current ? 'current' : null,
                $faq->post_title
            );
        }
        // if ( ! is_null($html) ) {
        //     $html = sprintf('<section class="faqs">%s</section>', $html);
        // }
        return $html;
    }
    return 'test';
}
add_shortcode( 'faq', 'faq_func' );

/* DON'T DELETE THIS CLOSING TAG */ ?>
