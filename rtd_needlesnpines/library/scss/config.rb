Encoding.default_external = "utf-8"
# Compass config file
# 1. Set this to the root of your project when deployed:
http_path = "/"

# 2. probably don't need to touch these
css_dir = "../css"
sass_dir = "./"
images_dir = "../images"
javascripts_dir = "../js"
environment = :development
relative_assets = true

# 3. You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded

# 4. When you are ready to launch your WP theme comment out (3) and uncomment the line below
output_style = :compressed

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = true

# don't touch this
preferred_syntax = :scss

# Runs the compiled CSS through Autoprefixer (Rails version), reads from 'browserslist'
# require 'autoprefixer-rails'

# on_stylesheet_saved do |file|
#   css = File.read(file)
#   map = file + '.map'

#   if File.exists? map
#     result = AutoprefixerRails.process(css,
#       from: file,
#       to:   file,
#       map:  { prev: File.read(map), inline: false })
#     File.open(file, 'w') { |io| io << result.css }
#     File.open(map,  'w') { |io| io << result.map }
#   else
#     File.open(file, 'w') { |io| io << AutoprefixerRails.process(css) }
#   end
# end
