/*
 * Needles N Pines Scripts File
 * Author: Ben Weibel
 *
*/

/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
    var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
    return { width:x,height:y };
}
// setting the viewport width
var viewport = updateViewportDimensions();

/*
 * Throttle Resize-triggered Events
 * Wrap your actions in this function to throttle the frequency of firing them off, for better performance, esp. on mobile.
 * ( source: http://stackoverflow.com/questions/2854407/javascript-jquery-window-resize-how-to-fire-after-the-resize-is-completed )
*/
var waitForFinalEvent = (function () {
    var timers = {};
    return function (callback, ms, uniqueId) {
        if (!uniqueId) { uniqueId = "Don't call this twice without a uniqueId"; }
        if (timers[uniqueId]) { clearTimeout (timers[uniqueId]); }
        timers[uniqueId] = setTimeout(callback, ms);
    };
})();

// how long to wait before deciding the resize has stopped, in ms. Around 50-100 should work ok.
var timeToWaitForLast = 200;

/*
 * Regular jQuery in here.
 */
(function($){

    var loggedIn = false;
    var customizer = false;

    $(document).ready(function() {

        // Initialize the pointer events polyfill (click through, cross-browser)
            // PointerEventsPolyfill.initialize({});

        // Initialize the homepage slider pointer events polyfill (click through, cross-browser)
            // PointerEventsPolyfill.initialize({ selector:'.home-slider-overlay', mouseEvents: ['click'] });

        // set the viewport using the function above
            viewport = updateViewportDimensions();

        if( $('body').hasClass('logged-in') ) loggedIn = true;

        if( $('#wpadminbar').length ) {
            if( viewport.width < 1030 ) {
                $('#wpadminbar').css('display','none');
                $('html')[0].style.setProperty('margin-top','0','important');
            }
        }

        var custBody = $('body');

        if( $('body').hasClass('wp-customizer')) {
            custBody = window.frames[0].document.getElementsByTagName("body")[0];
        }

        if( $( custBody ).hasClass('wp-customizer') ) customizer = true;

        if( customizer ) {
            $('#wpadminbar').css('display','none');
            $('html')[0].style.setProperty('margin-top','0','important');
        }

        var mobileMenuSlide = new $.slidebars({
            siteClose:              true,       // true or false
            disableOver:            1029,       // integer or false
            scrollLock:             true,       // true or false
            onOpen: function(){
                $('.material-icon').addClass('arrow');
            },
            onClose: function(){
                $('.material-icon').removeClass('arrow');
            }
        });

        var evbaseSlidebar = $('.sb-right');

        $('.material-icon').click(function() {
            mobileMenuSlide.slidebars.toggle('right');

            evbaseSlidebar.toggleClass('slidebar-zindex');
        });

        // Listings
        $('.title').click(function(){
            $(this).parent().toggleClass('open');
        })

        $('a[href*=\\#]:not([href=\\#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                viewport = updateViewportDimensions();
                var evOffset = 0;

                if ($('#wpadminbar').length) {
                    evOffset += 42;
                }

                if (target.length) {
                    $('html,body').animate({
                        scrollTop: ((target.offset().top) - evOffset)
                    }, 500);
                    return false;
                }
            }
        });

        /* Prevent default if menu links are "#". */
        $('.top-nav li a').each( function() {
        // $('.rtd_main-nav li a, .mobileNav li a').each( function() {
            var nav = $(this);
            if( nav.length > 0 ) {
                if( nav.attr('href') == '#' ) {
                    $(this).click(
                        function(e) {
                            e.preventDefault();
                        }
                    );
                }
            }
        });

        // $('.mobile-nav li.menu-item-has-children a').click(function() {
        //     $(this).parent().toggleClass('open');
        // });

        // $('.mobile-nav li.menu-item-has-children').append('<img src="/wp-content/themes/rtd_base-theme-2016/library/images/mobile-dropdown-arrow.png" class="arrow-icon" />');

    }); /* end of as page load scripts */

    $(window).load(function() {

    }); /* end of as wait scripts */

    $(window).resize(function() {

        // set the viewport using the function above
        viewport = updateViewportDimensions();

        var custBody = $('body');

        if( $('body').hasClass('wp-customizer')) {
            custBody = window.frames[0].document.getElementsByTagName("body")[0];
        }

        if( $( custBody ).hasClass('wp-customizer') ) customizer = true;

        if( customizer ) {
            $('#wpadminbar').css('display','none');
            $('html')[0].style.setProperty('margin-top','0','important');
        }

        if( $('body').hasClass('logged-in') ) loggedIn = true;

        // if we're logged in, we wait the set amount (in function above) then fire the function
        if( loggedIn ) { waitForFinalEvent( function() {

            if( viewport.width < 1030 ) {
                $('#wpadminbar').css('display','none');
                $('html')[0].style.setProperty('margin-top','0','important');
            } else {
                $('#wpadminbar').css('display','block');
                $('html')[0].style.setProperty('margin-top','32px','important');
            }

        }, timeToWaitForLast); }

    }); /* end of as resize scripts */

    $(window).scroll(function() {

        // set the viewport using the function above
        viewport = updateViewportDimensions();

    });

})(window.jQuery);
