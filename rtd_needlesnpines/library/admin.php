<?php
/*
This file handles the admin area and functions.
It's turned on by default, comment out in the functions file if you don't want to use it.

Author: Ben Weibel
URL: https://redtaildesignco.com//
*/

/************* DASHBOARD WIDGETS *****************/

// disable default dashboard widgets
function disable_default_dashboard_widgets() {
    global $wp_meta_boxes;
    // unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);    // Right Now Widget
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);        // Activity Widget
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // Comments Widget
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);  // Incoming Links Widget
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);         // Plugins Widget

    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);       // Quick Press Widget
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);     // Recent Drafts Widget
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);           //
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);         //
}

// calling all custom dashboard widgets
function bones_custom_dashboard_widgets() {
    /*
    Be sure to drop any other created Dashboard Widgets
    in this function and they will all load.
    */
}


// removing the dashboard widgets
add_action( 'wp_dashboard_setup', 'disable_default_dashboard_widgets' );
// adding any custom widgets
add_action( 'wp_dashboard_setup', 'bones_custom_dashboard_widgets' );


/************* CUSTOM LOGIN PAGE *****************/

// calling your own login css so you can style it

//Updated to proper 'enqueue' method
//http://codex.wordpress.org/Plugin_API/Action_Reference/login_enqueue_scripts
function bones_login_css() {
    wp_enqueue_style( 'bones_login_css', get_template_directory_uri() . '/library/css/login.css', false );
}

// changing the logo link from wordpress.org to your site
function bones_login_url() {  return home_url(); }

// changing the alt text on the logo to show your site name
function bones_login_title() { return get_option( 'blogname' ); }

// calling it only on the login page
add_action( 'login_enqueue_scripts', 'bones_login_css', 10 );
add_filter( 'login_headerurl', 'bones_login_url' );
add_filter( 'login_headertitle', 'bones_login_title' );


/************* CUSTOMIZE ADMIN *******************/

// Custom Backend Footer
function bones_custom_admin_footer() {
    _e( '<span id="footer-thankyou">Developed by <a href="https://redtaildesignco.com/" target="_blank">RTD Athens</a></span>. ', 'rtdtheme' );
}

// adding it to the admin area
add_filter( 'admin_footer_text', 'bones_custom_admin_footer' );

// Sorry..
function admin_css(){ ?>
    <style>
        .wp-admin .wpseo_content_cell#sidebar-container,
        .wp-admin.toplevel_page_metaslider #screen-meta-links #screen-options-link-wrap,
        .wp-admin #metaslider_social,
        .wp-admin .list-roles .footer,
        .wp-admin .role-add-new .footer,
        .gold_plugins_settings .plugin_is_not_registered {
          display: none !important;
      }
    </style>
<?php
}
add_action( 'admin_head', 'admin_css' );

// Remove the welcome panel
remove_action( 'welcome_panel', 'wp_welcome_panel' );

// Removes from admin menu
add_action( 'admin_menu', 'rtd_remove_admin_menus' );
function rtd_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}
// Removes from post and pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}
// Removes from admin bar
function rtd_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'rtd_admin_bar_render' );

// Remove Admin Menu submenu items
add_action( 'admin_bar_menu', 'remove_wp_nodes', 999 );
function remove_wp_nodes()
{
    global $wp_admin_bar;
    $wp_admin_bar->remove_node( 'new-link' );
}

?>
