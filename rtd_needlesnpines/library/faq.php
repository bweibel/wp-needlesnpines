<?php
/*
Title: Government Custom Post Type
Authors: Ben Weibel, Will Presley
URL: https://redtaildesignco.com/
*/

// Government CPT
    $faq_labels = array(
        'name'                  => _x( 'FAQs', 'FAQs General Name', 'rtdtheme' ),
        'singular_name'         => _x( 'FAQ', 'FAQ Singular Name', 'rtdtheme' ),
        'menu_name'             => __( 'FAQs', 'rtdtheme' ),
        'name_admin_bar'        => __( 'FAQs', 'rtdtheme' ),
        'archives'              => __( 'Item Archives', 'rtdtheme' ),
        'parent_item_colon'     => __( 'Parent Item:', 'rtdtheme' ),
        'all_items'             => __( 'All FAQs', 'rtdtheme' ),
        'add_new_item'          => __( 'Add New FAQ', 'rtdtheme' ),
        'add_new'               => __( 'Add New', 'rtdtheme' ),
        'new_item'              => __( 'New FAQ', 'rtdtheme' ),
        'edit_item'             => __( 'Edit FAQ', 'rtdtheme' ),
        'update_item'           => __( 'Update FAQ', 'rtdtheme' ),
        'view_item'             => __( 'View FAQ', 'rtdtheme' ),
        'search_items'          => __( 'Search FAQ', 'rtdtheme' ),
        'not_found'             => __( 'Not found', 'rtdtheme' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'rtdtheme' ),
        'featured_image'        => __( 'Featured Image', 'rtdtheme' ),
        'set_featured_image'    => __( 'Set featured image', 'rtdtheme' ),
        'remove_featured_image' => __( 'Remove featured image', 'rtdtheme' ),
        'use_featured_image'    => __( 'Use as featured image', 'rtdtheme' ),
        'insert_into_item'      => __( 'Insert into item', 'rtdtheme' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'rtdtheme' ),
        'items_list'            => __( 'Items list', 'rtdtheme' ),
        'items_list_navigation' => __( 'Items list navigation', 'rtdtheme' ),
        'filter_items_list'     => __( 'Filter items list', 'rtdtheme' ),
    );
    $faq_args = array(
        'label'                 => __( 'FAQ', 'rtdtheme' ),
        'description'           => __( 'FAQ Description', 'rtdtheme' ),
        'labels'                => $faq_labels,
        'supports'              => array( 'title', 'editor', 'revisions'),
        'taxonomies'            => array(  ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 9,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'show_in_rest'          => false,
        'has_archive'           => false,
        'menu_icon'             => 'dashicons-format-status',
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        // 'rewrite'               => array(
        //                             'slug' => 'government',
        //                             'with_front' => false,
        //                         ),
    );
    register_post_type( 'needles_faq', $faq_args );


    /* DON'T DELETE THIS CLOSING TAG */ ?>
