<?php
/*
Title: RTD Theme Panel
Author: Ben Weibel
URL: https://redtaildesignco.com/
*/

function rtd_theme_customizer( $wp_customize ) {

    $wp_customize->remove_panel( 'wpseo_panel' );
    $wp_customize->remove_section( 'wpseo_breadcrumbs_customizer_section' );
    $wp_customize->remove_section( 'wpseo_social_customizer_section' );

    $wp_customize->remove_section( 'title_tagline' );
    $wp_customize->remove_section( 'colors' );
    $wp_customize->remove_section( 'header_image' );
    $wp_customize->remove_section( 'background_image' );
    $wp_customize->remove_section( 'nav' );
    $wp_customize->remove_section( 'static_front_page' );

    $wp_customize->add_section( 'rtdbase_all_section' , array(
        'title'                 => __( 'All Settings', 'rtdtheme' ),
        'priority'              => 10,
        'description'           => 'Site-wide settings',
    ) );

    $wp_customize->add_setting( 'rtdbase_show_search', array(
        'default'               => '1',
        'sanitize_callback'     => 'rtdbase_sanitize_checkbox',
    ) );

    $wp_customize->add_control( 'rtdbase_show_search_control', array(
        'settings'              => 'rtdbase_show_search',
        'label'                 => __( 'Show/Hide the search box' ),
        'section'               => 'rtdbase_all_section',
        'type'                  => 'checkbox',
    ) );



    $wp_customize->add_section( 'rtdbase_contact_section', array(
        'title'                 => __( 'Contact Info & Display', 'rtdtheme' ),
        'priority'              => 20,
        'description'           => 'Hide or show the contact bar and update the phone number displayed',
    ) );

    $wp_customize->add_setting( 'rtdbase_show_topbar', array(
        'default'               => '1',
        'sanitize_callback'     => 'rtdbase_sanitize_checkbox',
    ) );

    $wp_customize->add_control( 'rtdbase_show_topbar_control', array(
        'settings'              => 'rtdbase_show_topbar',
        'label'                 => __( 'Show/Hide the contact bar' ),
        'section'               => 'rtdbase_contact_section',
        'type'                  => 'checkbox',
    ) );

    $wp_customize->add_setting( 'rtdbase_phone_number', array(
        'default'               => '800.321.8293',
        'sanitize_callback'     => 'rtdbase_sanitize_text',
    ) );

    $wp_customize->add_control( 'rtdbase_phone_number_control', array(
        'settings'              => 'rtdbase_phone_number',
        'label'                 => __( 'Site or Company Phone Number:' ),
        'section'               => 'rtdbase_contact_section',
        'type'                  => 'text',
    ) );

    $wp_customize->add_setting( 'rtdbase_address', array(
        'default'               => '149 Pine Grove Dr. Nelsonville, Ohio 45764',
        'sanitize_callback'     => 'rtdbase_sanitize_text',
    ) );

    $wp_customize->add_control( 'rtdbase_address_control', array(
        'settings'              => 'rtdbase_address',
        'label'                 => __( 'Address:' ),
        'section'               => 'rtdbase_contact_section',
        'type'                  => 'text',
    ) );


    $wp_customize->add_section( 'rtdbase_logo_section' , array(
        'title'                 => __( 'Header Logo', 'rtdtheme' ),
        'priority'              => 30,
        'description'           => 'Upload a logo to replace the default site name and description in the header',
    ) );

    $wp_customize->add_setting( 'rtdbase_logo' );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'rtdbase_logo', array(
        'label'                 => __( 'Logo', 'rtdtheme' ),
        'section'               => 'rtdbase_logo_section',
        'settings'              => 'rtdbase_logo',
    ) ) );



    $wp_customize->add_section( 'rtdbase_social_section' , array(
        'title'                 => __( 'Social Media', 'rtdtheme' ),
        'priority'              => 40,
        'description'           => 'Change social media URLs',
    ) );

}
add_action( 'customize_register', 'rtd_theme_customizer' );

function rtdbase_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}

function rtdbase_sanitize_checkbox( $input ) {
    if ( $input == 1 ) {
        return 1;
    } else {
        return '';
    }
}

// [rtdbase_phone_number] SHORTCODE (for footer, for example)
function phone_num_func( $atts ){
    $phone_num = get_theme_mod( 'rtdbase_phone_number', 'No phone number has been saved yet.' );
    $dial_num = str_replace(array('.', ','), '' , $phone_num);
    return $phone_num;
}
add_shortcode( 'rtdbase_phone_number', 'phone_num_func' );



/* DON'T DELETE THIS CLOSING TAG */ ?>
