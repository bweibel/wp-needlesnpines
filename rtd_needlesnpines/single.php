<?php
/*
    Single Template
*/
get_header(); ?>

            <div class="standardpage-wrap" id="content">

                <div id="inner-content" class="wrap cf">

                    <div class="page-container m-all t-all d-all">

                        <div class="featured-image">
                            <?php
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail('full');
                                } else { ?>
                                    <img width="1200" height="250" src="<?php echo get_stylesheet_directory_uri() . '/library/images/placeholders/1200by250-placeholder.png'; ?>" class="attachment-full wp-post-image" alt="placeholder_header">
                                <?php } ?>
                        </div>

                        <main id="main" class="standard-content m-all t-2of3 d-3of4 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

                            <?php if ( function_exists('yoast_breadcrumb') ) {
                                yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                            } ?>

                            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                                <article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">

                                    <header class="page-header">

                                        <h1 class="page-title"><?php the_title(); ?></h1>

                                    </header>

                                        <section class="page-content cf" itemprop="articleBody">
                                            <?php the_content(); ?>
                                        </section>

                                    </article>

                            <?php endwhile; ?>

                            <?php else : ?>

                                <article id="post-not-found" class="hentry cf">
                                    <header class="article-header">
                                        <h1><?php _e( 'Not Found!', 'rtdtheme' ); ?></h1>
                                    </header>
                                </article>

                            <?php endif; ?>

                        </main>

                        <?php get_sidebar(); ?>

                    </div>

                </div>

            </div>

<?php get_footer(); ?>
