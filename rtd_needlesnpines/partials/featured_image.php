<div class="featured-image">
    <?php
        if ( has_post_thumbnail() ) {
            the_post_thumbnail('full');
        } else {
            if ( is_page_template('page-homepage.php') ) {
                // Do nothing.
            } else { ?>
                <img width="1200" height="250" src="<?php echo get_stylesheet_directory_uri() . '/library/images/placeholders/1200by250-placeholder.png'; ?>" class="attachment-full wp-post-image" alt="placeholder_header">
            <?php } 
            } ?>
</div>