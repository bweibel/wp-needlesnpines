<?php
/*
Title: Functions
Author: Ben Weibel
URL: https://redtaildesignco.com/
*/

/***************************************************
*
* Overrides of Standard Features
*
***************************************************/

    /***************************************************
    * Function: modify_jquery()
    * Replace WP jQuery with Google CDN version
    * Docs: https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-replace-wordpress-jquery
    ***************************************************/
        function modify_jquery() {
            if (!is_admin()) {
                remove_action( 'wp_footer', 'wp_func_jquery' );
                // Comment out the next two lines to load the local copy of jQuery
                wp_deregister_script('jquery');
                wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js', false, '1.12.2');
                wp_enqueue_script('jquery');
            }
        }
        add_action('init', 'modify_jquery');

    /***************************************************
    * Function: post_remove()
    * Hide the 'Posts' Admin sidebar menu option by default (comment out to show, for news/events/etc)
    * Docs: https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-hide-admin-menu-posts-option
    ***************************************************/
        // function post_remove () {
        //    remove_menu_page('edit.php');
        // }
        // add_action('admin_menu', 'post_remove');

/**** (/END) Overrides of Standard Features ****/


/***************************************************
*
* Bones Defaults
*
***************************************************/

    // LOAD BONES CORE (if you remove this, the theme will break) (https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-load-bones-core)
        require_once( 'library/bones.php' );

    // CUSTOMIZE THE WORDPRESS ADMIN (https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-load-admin-dash-adjustments)
        require_once( 'library/admin.php' );

    /***************************************************
    * Function: bones_ahoy()
    * LAUNCH BONES
    * Docs: (https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-launch-bones
    ***************************************************/
        function bones_ahoy() {

            //Allow editor style.
            add_editor_style( get_stylesheet_directory_uri() . '/library/css/editor-style.css' );

            // launching operation cleanup
            add_action( 'init', 'bones_head_cleanup' );
            // A better title
            add_filter( 'wp_title', 'rw_title', 10, 3 );
            // remove WP version from RSS
            add_filter( 'the_generator', 'bones_rss_version' );
            // remove pesky injected css for recent comments widget
            add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
            // clean up comment styles in the head
            add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );
            // clean up gallery output in wp
            add_filter( 'gallery_style', 'bones_gallery_style' );

            // enqueue base scripts and styles
            add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );
            // ie conditional wrapper

            // launching this stuff after theme setup
            bones_theme_support();

            // adding sidebars to Wordpress
            add_action( 'widgets_init', 'rtd_register_sidebars' );

            // cleaning up random code around images
            add_filter( 'the_content', 'bones_filter_ptags_on_images' );

            // cleaning up excerpt
            add_filter( 'excerpt_more', 'bones_excerpt_more' );

        } /* end bones ahoy */

        // let's get this party started
        add_action( 'after_setup_theme', 'bones_ahoy' );

    // Enable support for new theme features.
    function theme_slug_setup() {
        // Enable new proper WP titles
            add_theme_support( 'title-tag' );

        // Enable support for HTML5 markup.
           add_theme_support( 'html5', array(
               'search-form'
           ) );
    }
    add_action( 'after_setup_theme', 'theme_slug_setup' );

/**** (/END) Bones Defaults ****/

/***************************************************
*
* Redtail Functions
*
***************************************************/

    /***************************************************
    * Register Widget Areas
    * Docs: https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-register-widget-areas
    ***************************************************/
        function rtd_register_sidebars() {
            // Main sidebar
            register_sidebar(array(
                'id' => 'sidebar1',
                'name' => __( 'Main Sidebar', 'rtdtheme' ),
                'description' => __( 'The sidebar on the right side of all standard pages.', 'rtdtheme' ),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h2 class="widgettitle">',
                'after_title' => '</h2>',
            ));

            // Footer Area (duplicate and adjust SCSS accordingly for multiple "areas" [see SFS theme])
            register_sidebar(array(
                'id' => 'footerarea1',
                'name' => __( 'Footer Area', 'rtdtheme' ),
                'description' => __( 'Information in the footer of the site.', 'rtdtheme' ),
                'before_widget' => '<div id="%1$s" class="widget %2$s footerarea1">',
                'after_widget' => '</div>',
                'before_title' => '<h2>',
                'after_title' => '</h2>',
            ));
            register_sidebar(array(
                'id' => 'footerarea2',
                'name' => __( 'Footer Area', 'rtdtheme' ),
                'description' => __( 'Information in the footer of the site.', 'rtdtheme' ),
                'before_widget' => '<div id="%1$s" class="widget %2$s footerarea2">',
                'after_widget' => '</div>',
                'before_title' => '<h2>',
                'after_title' => '</h2>',
            ));
            register_sidebar(array(
                'id' => 'footerarea3',
                'name' => __( 'Footer Area', 'rtdtheme' ),
                'description' => __( 'Information in the footer of the site.', 'rtdtheme' ),
                'before_widget' => '<div id="%1$s" class="widget %2$s footerarea3">',
                'after_widget' => '</div>',
                'before_title' => '<h2>',
                'after_title' => '</h2>',
            ));
        } // don't remove this bracket!

    /***************************************************
    * Function: bones_fonts()
    * Properly enqueue Google Web Fonts (concatenate within one request for additional fonts)
    * Docs: https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-properly-enqueue-google-web-fonts
    ***************************************************/
        function bones_fonts() {
            wp_enqueue_style('googleFonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800');
        }
        add_action('wp_enqueue_scripts', 'bones_fonts');

    /***************************************************
    * Add Custom Thumbnail sizes
    * (will be generated every time an image is added to the Media Library, can be called using something like mobble [see ACEnet theme] to load certain images on certain device types)
    * Docs: https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-add-custom-auto-thumbnail-sizes
    ***************************************************/
        // add_image_size( 'rtd-thumb-300', 300, 168, true ); // 300 by 168 pixels

        // add_image_size( 'rtd-featured-thumb-800', 800, 400, true ); // 800 by 400 pixels

        // add_image_size( 'rtd-featured-thumb-400', 400, 250, true ); // 400 by 250 pixels

    /***************************************************
    * Change the 'default' category to something other than 'Uncategorized'
    * (will be generated every time an image is added to the Media Library, can be called using something like mobble [see ACEnet theme] to load certain images on certain device types)
    * Docs: https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-change-uncategorized-to-something-else
    ***************************************************/
        // Uncategorized ID is always 1
            // wp_update_term(1, 'category', array(
            //     'name' => 'News',
            //     'slug' => 'News',
            //     'description' => 'The latest news.'
            // ));

    /***************************************************
    * Function: rtd_imagelink_setup()
    * Stop Image Auto-Link on Upload
    * Docs: https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-stop-image-auto-link-on-upload
    ***************************************************/
        function rtd_imagelink_setup() {
            $image_set = get_option( 'image_default_link_type' );

            if ($image_set !== 'none') {
                update_option('image_default_link_type', 'none');
            }
        }
        add_action('admin_init', 'rtd_imagelink_setup', 10);

    /***************************************************
    * Function: custom_read_more()
    * RTD Custom Read More (Flexible)
    * Docs: https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-ev-custom-read-more-flexible
    ***************************************************/
        // Output the Read More link.
        function custom_read_more() {
            return '... <a class="excerpt-read-more" href="'.get_permalink(get_the_ID()).'">Read More</a>';
        }

        // Output the excerpt at the specified length (defaults to 130 words)
        function improved_trim_excerpt($text, $length=130) {
            global $post;

            if ( '' == $text ) {
                $text = get_the_content('');
                $text = apply_filters('the_content', $text);
                $text = str_replace('\]\]\>', ']]&gt;', $text);
                $text = preg_replace('@<script[^>]*?>.*?</script>@si', '', $text);
                $text = strip_tags($text, '<p>');
                $excerpt_length = $length;
                $words = explode(' ', $text, $excerpt_length + 1);
                if (count($words)> $excerpt_length) {
                        array_pop($words);
                        // array_push($words, '[...]');
                        array_push($words, custom_read_more());
                        $text = implode(' ', $words);
                }
            }

            return $text;
        }

        // Replace the Wordpress default get_the_excerpt function (feel free to comment these two lines and just call the function above elsewhere if you need the default function too)
        remove_filter('get_the_excerpt', 'wp_trim_excerpt');
        add_filter('get_the_excerpt', 'improved_trim_excerpt');


    /***************************************************
    * Function: custom_pagination($numpages = '', $pagerange = '', $paged='')
    * RTD Custom Pagination
    * Docs: (https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-ev-custom-pagination
    ***************************************************/
        function custom_pagination($numpages = '', $pagerange = '', $paged='') {

            if (empty($pagerange)) {
                $pagerange = 2;
            }

            global $paged;
            if (empty($paged)) {
                $paged = 1;
            }
            if ($numpages == '') {
                global $wp_query;
                $numpages = $wp_query->max_num_pages;
                if(!$numpages) {
                    $numpages = 1;
                }
            }

            $pagination_args = array(
                'base'            => get_pagenum_link(1) . '%_%',
                'format'          => 'page/%#%',
                'total'           => $numpages,
                'current'         => $paged,
                'show_all'        => False,
                'end_size'        => 1,
                'mid_size'        => $pagerange,
                'prev_next'       => True,
                'prev_text'       => __('&laquo; Previous'),
                'next_text'       => __('Next &raquo;'),
                'type'            => 'plain',
                'add_args'        => false,
                'add_fragment'    => ''
                );

            $paginate_links = paginate_links($pagination_args);

            if ($paginate_links) {
                echo "<nav class='custom-pagination cf'>";
                echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
                echo $paginate_links;
                echo "</nav>";
            }
        }

    /***************************************************
    * Function: rtd_custom_login_message()
    * Add a Custom Login Message
    * Docs: https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-add-a-custom-login-message
    ***************************************************/
        function rtd_custom_login_message() {
            $message = "<div style='font-size: 160%; line-height: 1.2; margin-bottom: 1em;'><strong>Note.<br /><br /><span style='color: red;''>This page is for <u>website administrators only!</u></span></strong></div>";
            return $message;
        }

        add_filter('login_message', 'rtd_custom_login_message');

    /***************************************************
    * Filter: single_template
    * Apply Single Templates Per-Category (by slug)
    * Docs: https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-add-a-custom-login-message
    ***************************************************/
        add_filter('single_template', create_function(
            '$the_template',
            'foreach( (array) get_the_category() as $cat ) {
                if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") )
                return TEMPLATEPATH . "/single-{$cat->slug}.php"; }
            return $the_template;' )
        );

    /***************************************************
    * Function: posts_orderby_lastname
    * Query sort string for second word (for use in Query, to sort by last names, for example)
    * Docs: https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-query-sort-string-for-second-word
    ***************************************************/

        function posts_orderby_lastname ($orderby_statement) {
            $orderby_statement = "RIGHT(post_title, LOCATE(' ', REVERSE(post_title)) - 1) ASC";
            return $orderby_statement;
        }

/**** (/END) EV Functions ****/

/***************************************************
*
* Plugin Functions
*
***************************************************/

    /***************************************************
    * Function: posts_orderby_lastname
    * Query sort string for second word (for use in Query, to sort by last names, for example)
    * Docs: https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-query-sort-string-for-second-word
    ***************************************************/

    // Callback function to insert 'styleselect' into the $buttons array
    function rtd_mce_buttons_2( $buttons ) {
        array_unshift( $buttons, 'styleselect' );
        return $buttons;
    }
    // Register our callback to the appropriate filter
    add_filter('mce_buttons_2', 'rtd_mce_buttons_2');

    // Add the Button CSS to the Dropdown Menu
    // Callback function to filter the MCE settings
    function rtd_mce_before_init_insert_formats( $init_array ) {

        // Define the style_formats array
        $style_formats = array(

            // Each array child is a format with it's own settings
            array(
                'title' => 'Bigger Normal Text - [p, li, span, a, div]',
                'selector' => 'p, li, span, a, div',
                'classes' => 'bodytext-bigger',
            ),
            array(
                'title' => 'Link Button light - [a]',
                'selector' => 'a',
                'classes' => 'needles-button-light',
            ),
            array(
                'title' => 'Link Button dark - [a]',
                'selector' => 'a',
                'classes' => 'needles-button-dark',
            )
        );

        // Insert the array, JSON ENCODED, into 'style_formats'
        $init_array['style_formats'] = json_encode( $style_formats );
        return $init_array;
    }

    // Attach callback to 'tiny_mce_before_init'
    add_filter( 'tiny_mce_before_init', 'rtd_mce_before_init_insert_formats' );


    // Fallback for contact form 7 date picker
    add_filter( 'wpcf7_support_html5_fallback', '__return_true' );
/**** (/END) EV Functions ****/

    /***************************************************
    * Function: ms_plugin_init()
    * Master Slider Load Check
    * Docs: https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-master-slider-load-check
    ***************************************************/
        function ms_plugin_init() {
            if( class_exists( 'Master_Slider' ) ) { // If the plugin is installed and activated
                update_option('ms_loaded', "1"); // The ms_loaded option is true.
            } else {
                update_option('ms_loaded', "0"); // else false.
            }
        }

        add_action( 'after_setup_theme', 'ms_plugin_init' ); // Hook into the end of theme setup.

    /***************************************************
    * Function: ms_plugin_init()
    * ikFacebook Load-Check
    * Docs: https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-ikfacebook-load-check
    ***************************************************/
        function fb_plugin_init() {
            if( class_exists( 'ikFacebook' ) ) {
                update_option('fb_loaded', "1");
            } else {
                update_option('fb_loaded', "0");
            }
        }

        add_action( 'after_setup_theme', 'fb_plugin_init' );

    // Multiple Thumbnails per Post/Page/etc Example (https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-multiple-thumbnails-per-page-post-etc-example)
        // IF MULTIPLE POST THUMBNAILS IS INSTALLED, add new meta field and give it a label and ID (will need activated for certain post types/templates using ACF or similar)
        // if (class_exists('MultiPostThumbnails')) {
        //     new MultiPostThumbnails(
        //         array(
        //             'label' => 'Homepage Thumbnail Image',
        //             'id' => 'homepage-thumb-image'
        //         )
        //     );
        // }

    // Fix ACF Textarea CSS (https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-fix-acf-textarea-css)
        add_filter('admin_head','textarea_temp_fix');
        function textarea_temp_fix() {
            echo '<style type="text/css">.acf_postbox .field textarea {min-height:0 !important;}</style>';
        }

    /*************************************************************************
    // ACF Google Map Stuff
    // Uncomment this if using the ACF google map module. Be sure to swap out the API key
    /************************************************************************/

    // Fix ACF Textarea CSS (https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-fix-acf-textarea-css)
        // function map_module_load() {
        //     if( is_page_template('page-map.php') ){
        //          wp_enqueue_script( 'map-module', get_stylesheet_directory_uri() . '/library/js/map.js', array( 'jquery' ), '', true );
        //          wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCPiI9r0hhIn5rfd5gK4JxZdVQGB6u3LBw','', true);
        //     }
        // }
        // add_action('wp_enqueue_scripts', 'map_module_load');

        // function textarea_temp_fix() {
        //     echo '<style type="text/css">.acf_postbox .field textarea {min-height:0 !important;}</style>';
        // }
        // add_filter('admin_head','textarea_temp_fix');

        // function my_acf_google_map_api( $api ){
        //     $api['key'] = 'AIzaSyCPiI9r0hhIn5rfd5gK4JxZdVQGB6u3LBw';
        //     return $api;
        // }
        // add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

/**** (/END) Functions dependent on plugin usage ****/

/**** Debugging Functions (ONLY COMMENT OUT AFTER READING THE WIKI!!!) ****/
    // Print All Enqueued Script Handles (https://bitbucket.org/electronicvision-ev/wp-ev-base-theme/wiki/Functions#markdown-header-print-all-enqueued-script-handles)
        // function wpa54064_inspect_scripts() {
        //     global $wp_scripts;
        //     foreach( $wp_scripts->queue as $handle ) :
        //         echo $handle,' ';
        //     endforeach;
        // }
        // add_action( 'wp_print_scripts', 'wpa54064_inspect_scripts' );

/**** (/END) Debugging Functions ****/

/**** Load Additional Files (Shortcodes, Custom Post Types, EV Theme Panel, etc) ****/
    // Load EV Shortcodes
        require_once( 'library/rtd-shortcodes.php' );

    // Load EV Theme Panel
        require_once( 'library/theme-panel.php' );

    // Load Staff Custom Post Type
        require_once( 'library/faq.php' );

/**** (/END) Load Additional Files ****/

/* DON'T DELETE THIS CLOSING TAG */ ?>
