                <div id="sidebar1" class="sidebar m-all t-1of3 d-1of4 last-col cf" role="complementary">

                    <?php if ( is_active_sidebar( 'sidebar1' ) ) : ?>

                        <?php dynamic_sidebar( 'sidebar1' ); ?>

                    <?php else : ?>

                        <div class="no-widgets">
                        </div>

                    <?php endif; ?>

                </div>
