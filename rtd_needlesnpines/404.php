<?php get_header(); ?>

            <div class="standardpage-wrap" id="content">

                <div id="inner-content" class="wrap cf">

                    <div class="page-container m-all t-all d-all">

                        <?php get_template_part( 'partials/featured_image' ); ?>

                        <main id="main" class="standard-content m-all t-2of3 d-3of4 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

                            <?php if ( function_exists('yoast_breadcrumb') ) {
                                yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                            } ?>

                            <article id="post-not-found" class="hentry cf">

                                <header class="article-header">

                                    <h1><?php _e( '404 - Not Found', 'rtdtheme' ); ?></h1>

                                </header>

                                <section class="entry-content">

                                    <p><?php _e( 'The page you were looking for was not found, use the menu above to navigate the site!', 'rtdtheme' ); ?></p>

                                </section>

                            </article>

                        </main>

                    <?php get_sidebar(); ?>

                    </div>

                </div>

            </div>

<?php get_footer(); ?>
