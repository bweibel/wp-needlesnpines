<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

    <head>
        <meta charset="utf-8">

        <?php // force Internet Explorer to use the latest rendering engine available ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <?php if ( ! function_exists( '_wp_render_title_tag' ) ) :
            function theme_slug_render_title() {
        ?>
        <title><?php wp_title(''); ?></title>
        <?php
            }
            add_action( 'wp_head', 'theme_slug_render_title' );
        endif; ?>

        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Use this link to generate the following: http://realfavicongenerator.net/ -->
        <!-- icons & favicons (more: http://jonathantneal.com/blog/understand-the-favicon/) -->

            <!--[if IE]>
                <link rel="shortcut icon" href="/favicon.ico">
            <![endif]-->

        <!-- (this block does not need changed) -->
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#4a4b29">
        <meta name="theme-color" content="#9b9057">

        <?php // wordpress head functions ?>
        <?php wp_head(); ?>
        <?php // end of wordpress head ?>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-69387226-18"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-69387226-18');
        </script>

    </head>

    <body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

        <div class="sb-slidebar sb-right">

            <nav class="mobile-nav-wrap" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
                <?php wp_nav_menu(array(
                    'container' => false,                           // remove nav container
                    'container_class' => 'menu cf',                 // class of container
                    'menu' => __( 'The Main Menu', 'rtdtheme' ),   // nav name
                    'menu_class' => 'nav mobile-nav cf',            // adding custom nav class
                    'theme_location' => 'main-nav',                 // where it's located in the theme
                        'before' => '',                             // before the menu
                        'after' => '',                              // after the menu
                        'link_before' => '',                        // before each link
                        'link_after' => '',                         // after each link
                    'depth' => 0,                                   // limit the depth of the nav
                    'fallback_cb' => ''                             // fallback function (if there is one)
                )); ?>
            </nav>

        </div>

        <header class="main-header full-wrap sb-slide" role="banner" itemscope itemtype="http://schema.org/WPHeader">

        <?php if( get_theme_mod( 'rtdbase_show_topbar' ) == '1' ) { ?>
            <div class="contact-bar">

                <?php
                    $phone_num = get_theme_mod( 'rtdbase_phone_number', 'No phone number has been saved yet.' );
                    $dial_num = str_replace(array('.', ','), '' , $phone_num);
                    // $address = get_theme_mod('rtdbase_address', 'No address found');
                    $address = '149 Pine Grove Dr. Nelsonville, Ohio 45764';
                ?>

                <div class="wrap">
                    <p class="contact-text">
                        <span class="phone-icon"></span>
                        <span id="phone-number"><a href="tel:+1<?php echo $dial_num; ?>"><?php echo $phone_num; ?></a></span>
                        <span class="map-icon"></span>
                        <span id="address"><?php echo $address; ?></span>
                    </p>
                </div>
            </div>
        <?php } ?>

            <div id="inner-header" class="wrap cf">

                <?php if ( get_theme_mod( 'rtdbase_logo' ) ) : ?>
                    <div class='header-logo'>
                        <a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='Return to the <?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> homepage.' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'rtdbase_logo' ) ); ?>' alt='Return to the <?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> homepage.'></a>
                    </div>
                <?php else : ?>
                    <h1 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><?php bloginfo( 'name' ); ?></a></h1>
                <?php endif; ?>

                <div class="header-right">
                    <!-- Search & Login link -->
                    <div class="above-nav">

                        <?php if( get_theme_mod( 'rtdbase_show_search' ) == '1' ) get_search_form(); ?>

                    </div>

                    <nav class="top-nav-wrap cf" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
                        <?php wp_nav_menu(array(
                            'container' => false,                           // remove nav container
                            'container_class' => 'menu cf',                 // class of container
                            'menu' => __( 'The Main Menu', 'rtdtheme' ),  // nav name
                            'menu_class' => 'nav top-nav',                  // adding custom nav class
                            'theme_location' => 'main-nav',                 // where it's located in the theme
                                'before' => '',                             // before the menu
                                'after' => '',                              // after the menu
                                'link_before' => '',                        // before each link
                                'link_after' => '',                         // after each link
                            'depth' => 0,                                   // limit the depth of the nav
                            'fallback_cb' => ''                             // fallback function (if there is one)
                        )); ?>
                    </nav>
                </div>

                <div class="material-icon hamburger" id="mobile-menu-btn">
                    <span class="first"></span>
                    <span class="second"></span>
                    <span class="third"></span>
                </div>

            </div>

        </header>

        <div id="sb-site" class="full-wrap">
